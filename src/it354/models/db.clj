(ns it354.models.db
  (:use monger.operators)
  (:require [monger.core :as mg]
            [monger.collection :as mc])
  (:import [org.bson.types ObjectId]))

(mg/connect!)

(mg/set-db! (mg/get-db "it354"))


(defn get-restaurants []
  (map
    (fn [obj] (dissoc obj :_id))
    (mc/find-maps "restaurants" {})))

(defn update-restaurant [restaurant_name address city state zip]
  (do
    (mc/update "restaurants"
      {:restaurant_name restaurant_name}
      {:restaurant_name restaurant_name
       :address address
       :city city
       :state state
       :zip zip}
      :upsert true)
    (get-restaurants)))

(defn rem-restaurant [restaurant_name]
  (do
    (mc/remove "restaurants"
      {:restaurant_name restaurant_name})
    (get-restaurants)))


(comment
  (update-restaurant
    "Gokul Indian Restaurant"
    "6101 Delmar Blvd"
    "Saint Louis"
    "MO"
    "63132")

  (update-restaurant
    "Little Italy Restaurant"
    "6130 E 82nd St"
    "Indianapolis"
    "IN"
    "46250")

  (update-restaurant
    "Lizard's Thicket"
    "4616 Augusta Rd"
    "Lexington"
    "SC"
    "29073")

  (update-restaurant
    "Medici"
    "120 W North St"
    "Normal"
    "IL"
    "61761")

  (update-restaurant
    "Radio Maria"
    "119 N Walnut St"
    "Champaign"
    "IL"
    "61820")

  (update-restaurant
    "Smoke BBQ"
    "123 Main"
    "Peoria"
    "IL"
    "61701")

  (update-restaurant
    "Bastas'"
    "955 West Jackson Street"
    "Morton"
    "IL"
    "61550")

  (rem-restaurant "Bastas'")
  (get-restaurants)
  (mc/remove "restaurants" {})
)

