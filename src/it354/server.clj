(ns it354.server
  (:require [noir.server :as server]
            [it354.views index layout utils]
            [cheshire.core :as json]
            [it354.models db]))

(server/load-views-ns 'it354.views)

(defn -main [& m]
  (let [mode (keyword (or (first m) :dev))
        port (Integer. (get (System/getenv) "PORT" "8081"))]
    (server/start port {:mode mode
                        :ns 'it354})))

(defn fix-base-url [handler]
  (fn [request]
    (with-redefs [noir.options/resolve-url 
                  (fn [url]
                    ;prepend context to the relative URLs
                    (if (.contains url "://")
                      url (str (:context request) url)))]
      (handler request))))


(defn json-body [handler]
  (fn [req]
    (let [neue (if (= "application/json" (get-in req [:headers "content-type"]))
        (update-in req [:params] merge (json/decode (slurp (:body req)) true)) req)]
      (handler neue))))

(server/add-middleware json-body)

(def base-handler
    (server/gen-handler {:ns 'it354}))

(def handler (-> base-handler fix-base-url))
(comment
 (def local-jetty 
   (server/start 8081))
)
