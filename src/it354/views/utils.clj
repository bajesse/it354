(ns it354.views.utils
  (:use [noir.core :only [defpartial]])
  (:require [noir.request :as request]))

(defn add-context [url]
  (str (:context (request/ring-request)) url))

(defpartial include-less [& styles]
  (for [style styles]
    [:link {:rel "stylesheet/less" :href
      (add-context style)}]))

(defpartial include-css [& styles]
  (for [style styles]
    [:link {:rel "stylesheet" :type "text/css" :href
      (add-context style)}]))

(defpartial include-js [& scripts]
  (for [script scripts]
    [:script {:type "text/javascript" :src
      (add-context script)}]))

(defpartial inline-js [content]
    [:script {:type "text/javascript"} content])

(defpartial external-js [& scripts]
  (for [script scripts]
    [:script {:type "text/javascript" :src script}]))

(defpartial inline-css [content]
  [:style {:type "text/css"} content])

(defpartial comment-html [& content]
  "<!--"
  content
  "-->")

(defpartial comment-ie-hidden [condition & content]
  "<!--[if " condition "]>"
  content
  "<![endif]-->")

(defpartial comment-ie-visible [condition & content]
    "<!--[if " condition "]><!-->"
    content
    "<!--<![endif]-->")
