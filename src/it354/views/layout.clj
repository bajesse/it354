(ns it354.views.layout
  (:use [noir.core :only [defpartial]]
        [hiccup.page :only [include-css include-js]])
  (:require [it354.views.utils :as utils]
            [it354.config :as config]))

(defpartial modernizr [& content]
  "<!doctype html>"
  (utils/comment-ie-hidden "lt IE 7" "<html class=\"no-js lt-ie9 lt-ie8 lt-ie7\" lang=\"en\">")
  (utils/comment-ie-hidden "IE 7" "<html class=\"no-js lt-ie9 lt-ie8\" lang=\"en\">")
  (utils/comment-ie-hidden "IE 8" "<html class=\"no-js lt-ie9\" lang=\"en\">")
  (utils/comment-ie-visible "gt IE 8" "<html class=\"no-js\" lang=\"en\">")
  content
  "</html>")

(defpartial page-head [title]
  [:head
    [:meta {:charset "utf-8"}]
    [:meta {:http-equiv "X-UA-Compatible" :content "IE=edge,chrome=1"}]
    [:meta {:name "description" :content config/DESCRIPTION}]
    [:meta {:name "author" :content config/AUTHOR}]
    [:meta {:name "viewport" :content "width=device-width"}]
    [:title title]
    [:link {:href "http://fonts.googleapis.com/css?family=Droid+Sans:400,700|Droid+Sans+Mono|Droid+Serif:400,700,400italic,700italic" :rel "stylesheet" :type "text/javascript"}]
    (utils/include-less "/less/bootstrap/bootstrap.less")
    (utils/include-css "/fancybox/jquery.fancybox.css?v=2.1.3"
                       "/vancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5")
    (utils/include-js "/js/libs/less-1.3.0.min.js"
                      "/js/libs/modernizr-2.6.1-respond-1.1.0.min.js")])

(defpartial page-bottom []
  (utils/external-js "//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js" 
                     "//ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js")
  (utils/inline-js
    (str "window.jQuery || document.write('<script src=\"" (utils/add-context "/js/libs/jquery-1.8.0.min.js\"><\\/script>')")))
  (utils/include-js "/fancybox/jquery.fancybox.pack.js?v=2.1.3")
  (utils/external-js "//ajax.googleapis.com/ajax/libs/angularjs/1.0.2/angular.min.js" "http://maps.googleapis.com/maps/api/js?key=AIzaSyCeCD6tndDv9DX4rT_VWd_yTbmqTkTHEMM&sensor=false")
  (utils/include-js "/js/libs/bootstrap.js"
                    "/js/script.js")
  (utils/inline-js (str "var context = '" (utils/add-context "';"))))

(defpartial layout [title & content]
  (modernizr
    (page-head title)
    [:body#ng-app {:ng-app "brianApp"}
      content
      (page-bottom)]))
