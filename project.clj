(defproject it354 "0.1.0"
            :plugins [[lein-ring "0.7.5"]]
            :ring {:handler it354.server/handler}
            :description "The first assignment for IT354"
            :dependencies [[org.clojure/clojure "1.4.0"]
                           [noir "1.3.0-beta10"]
                           [cheshire "5.0.1"]
                           [com.novemberain/monger "1.2.0"]
                           ;[korma "0.3.0-beta11"]
                           [mysql/mysql-connector-java "5.1.21"]]
            :main it354.server)
