/*global angular:true */
'use strict';
angular.module('brianApp', ['brianApp.controllers']).config(
    function($routeProvider) {
        $routeProvider.when('/home', {
            templateUrl: context + '/partials/home',
            controller: 'home'
        });
        $routeProvider.when('/resume', {
            templateUrl: context + '/partials/resume',
            controller: 'resume'
        });
        $routeProvider.when('/links', {
            templateUrl: context + '/partials/links',
            controller: 'links'
        });
        $routeProvider.when('/pipes', {
            templateUrl: context + '/partials/pipes',
            controller: 'pipes'
        });
        $routeProvider.when('/restaurants', {
            templateUrl: context + '/partials/restaurants',
            controller: 'restaurants'
        });
        $routeProvider.when('/videos', {
            templateUrl: context + '/partials/videos',
            controller: 'videos'
        });
        $routeProvider.when('/places', {
            templateUrl: context + '/partials/places',
            controller: 'places'
        });
        $routeProvider.when('/examples', {
            templateUrl: context + '/partials/examples',
            controller: 'examples'
        });
        $routeProvider.when('/gallery', {
            templateUrl: context + '/partials/gallery',
            controller: 'gallery'
        });
        $routeProvider.otherwise({
            redirectTo: '/home'
        });
    }
).directive('fancyBoxDirective', function() {
    return function($scope, $element, $attrs) {
        $($element).fancybox({
            'transitionIn'  : 'elastic',
            'transitionOut' : 'elastic',
            'easingIn'      : 'easeOutBack',
            'easingOut'     : 'easeInBack'
        });
    };
}).directive('sortableDirective', function() {
    return function($scope, $element, $attrs) {
        $($element).sortable({
            update: function () {
                $scope.pics = $($element).sortable('toArray');
            }
        });
    };
});
angular.module('brianApp.controllers', []).config(
    function ($controllerProvider) {
        $controllerProvider.register('home', function($rootScope) {
            activeChooser($rootScope, 0);
        });
        $controllerProvider.register('resume', function($rootScope) {
            activeChooser($rootScope, 1);
        });
        $controllerProvider.register('links', function($rootScope) {
            activeChooser($rootScope, 2);
            $('#links').collapse();
        });
        $controllerProvider.register('pipes', function($rootScope, $scope) {
            activeChooser($rootScope, 3);
        });
        $controllerProvider.register('restaurants', function($rootScope, $scope, $http) {
            activeChooser($rootScope, 4);
            $scope.column = 'restaurant_name';
            $scope.reverse = false;
            $scope.edit = false;
            $scope.getRestaurantList = function() {
                $http.get(context + '/db/restaurants').success(function(data) {
                    $scope.restaurantList = data;
                });
            };
            $scope.getRestaurantList();
            $('#restaurant_tabs a').click(function(event) {
                event.preventDefault();
                $(this).tab('show');
            });
            $('#add_restaurant_reset').click(function(event){
                event.preventDefault();
            });
            $scope.save = function() {
                if($scope.edit) {
                    console.log("Original: " + $scope.original.restaurant_name);
                    if(!angular.equals($scope.original.restaurant_name, $scope.restaurant_name)) {
                        console.log("Removing original: " + $scope.original.restaurant_name);
                        $scope.remove($scope.original.restaurant_name);
                    }
                }
                $http({
                    method: 'POST',
                    url: context + '/db/restaurants',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $.param({
                        restaurant_name: $scope.restaurant_name,
                        address: $scope.address,
                        city: $scope.city,
                        state: $scope.state,
                        zip: $scope.zip
                    })
                }).success(function(data) {
                    $scope.restaurantList = data;
                    $scope.reset();
                });
            }
            $scope.reset = function() {
                $scope.restaurant_name = '';
                $scope.address = '';
                $scope.city = '';
                $scope.state = '';
                $scope.zip = '';
                $('#restaurant_tabs a:first').tab('show');
                $scope.edit = false;
                $scope.original = null;
            };
            $scope.isClean = function() {
                if(!$scope.edit) {
                    return false;
                }
                return angular.equals($scope.original, {restaurant_name: $scope.restaurant_name,
                                                        address: $scope.address,
                                                        city: $scope.city,
                                                        state: $scope.state,
                                                        zip: $scope.zip});
            }
            $scope.update = function(n, a, c, s, z) {
                $scope.restaurant_name = n;
                $scope.address = a;
                $scope.city = c;
                $scope.state = s;
                $scope.zip = z;
                $scope.original = {restaurant_name: $scope.restaurant_name,
                                   address: $scope.address,
                                   city: $scope.city,
                                   state: $scope.state,
                                   zip: $scope.zip}
                $scope.edit = true;
                $('#restaurant_tabs a:nth-child(1)').tab('show');
            }
            $scope.remove = function(name) {
                $http({
                    method: 'DELETE',
                    url: context + '/db/restaurants',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $.param({
                        restaurant_name: name
                    })
                }).success(function(data) {
                    if(!$scope.edit) {
                        $scope.restaurantList = data;
                        $scope.reset();
                    }
                });
            };
        });
        $controllerProvider.register('videos', function($rootScope) {
            activeChooser($rootScope, 5);
        });
        $controllerProvider.register('places', function($rootScope, $scope, $http) {
            activeChooser($rootScope, 6);
            var normalCoords = new google.maps.LatLng(40.517988,-88.990914);
            var chicagoCoords = new google.maps.LatLng(41.878114,-87.627944);
            var normalWindow = null;
            var chicagoWindow = null;
            $http.get(context + '/partials/places/normal').success(function(data) {
                normalWindow = new google.maps.InfoWindow({
                    content: data
                });
                google.maps.event.addListener(normal, 'click', function() {
                    normalWindow.open(map, normal);
                });
            });
            $http.get(context + '/partials/places/chicago').success(function(data) {
                chicagoWindow = new google.maps.InfoWindow({
                    content: data
                });
                google.maps.event.addListener(chicago, 'click', function() {
                    chicagoWindow.open(map, chicago);
                });
            });
            var map = new google.maps.Map(document.getElementById("map_canvas"), {
                center: normalCoords,
                zoom: 7,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            var normal = new google.maps.Marker({
                position: normalCoords,
                map: map,
                title: 'Normal, IL'
            });
            var chicago = new google.maps.Marker({
                position: chicagoCoords,
                map: map,
                title: 'Chicago, IL'
            });
            $('#places_tabs a').click(function(event) {
                event.preventDefault();
            });
            $scope.showNormal = function() {
                map.panTo(normalCoords);
                normalWindow.open(map, normal);
                chicagoWindow.close();
            };
            $scope.showChicago = function() {
                map.panTo(chicagoCoords);
                chicagoWindow.open(map, chicago);
                normalWindow.close();
            };
        });
        $controllerProvider.register('examples', function($rootScope, $scope) {
            activeChooser($rootScope, 7);
            $('#carousel').carousel();
        });
        $controllerProvider.register('gallery', function($rootScope, $scope, $http) {
            activeChooser($rootScope, 8);
            $('#gallery_tabs a').click(function(event) {
                event.preventDefault();
                $(this).tab('show');
            });
            $scope.saveOrder = function() {
                console.log($scope.pics);
                $http.post(context + '/ws/picorder', {order: $scope.pics}, {
                    headers: {'Content-Type':'application/json'}
                }).success(function(data) {
                    $scope.pics = data;
                    $('#gallery_tabs a:first').tab('show');
                })
            }
            $scope.updateOrder = function() {
                $http.get(context + '/ws/picorder').success(function(data) {
                    $scope.pics = data;
                });
            };
            $scope.updateOrder();
        });
    }
);
function activeChooser($rootScope, n) {
    $rootScope.homeLink = '';
    $rootScope.resumeLink = '';
    $rootScope.linksLink = '';
    $rootScope.pipesLink = '';
    $rootScope.restaurantsLink = '';
    $rootScope.videosLink = '';
    $rootScope.placesLink = '';
    $rootScope.examplesLink = '';
    $rootScope.galleryLink = '';
    switch(n) {
        case 0:
            $rootScope.homeLink = 'active';
            break;
        case 1:
            $rootScope.resumeLink = 'active';
            break;
        case 2:
            $rootScope.linksLink = 'active';
            break;
        case 3:
            $rootScope.pipesLink = 'active';
            break;
        case 4:
            $rootScope.restaurantsLink = 'active';
            break;
        case 5:
            $rootScope.videosLink = 'active';
            break;
        case 6:
            $rootScope.placesLink = 'active';
            break;
        case 6:
            $rootScope.galleryLink = 'active';
            break;
        case 7:
            $rootScope.examplesLink = 'active';
            break;
    }
}
